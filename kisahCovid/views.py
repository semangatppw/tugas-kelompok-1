from django.shortcuts import render,redirect
from .models import Kisah as kisahh
from .forms import KisahForm

# Create your views here.
def submit_kisah(request):
    if request.method == "POST":
        form = KisahForm(request.POST)
        if form.is_valid():
            items = kisahh()
            items.nama = form.cleaned_data['nama']
            items.cerita = form.cleaned_data['cerita']
            items.save()
        return redirect("/kisahcovid")
    else:
        items = kisahh.objects.all() 
        form = KisahForm()
        response = {'items':items, 'form' : form}
        return render(request, "formkisah.html", response)

