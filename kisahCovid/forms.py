from django import forms

class KisahForm(forms.Form):

    nama = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'John Doe',
        'type' : 'text',
        'required': True,
    }))

    cerita = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Kisah saya saat pandemi Covid-19 adalah...',
        'type' : 'text',
        'required': True,
    }))