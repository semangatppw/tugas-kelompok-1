from django.test import TestCase, Client
from django.urls import resolve
from .views import submit_kisah
from .models import Kisah

# Create your tests here.
class TestHalamanKisah(TestCase):
    def test_kisah_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_using_func(self):
        found = resolve('/kisahcovid/')
        self.assertEqual(found.func, submit_kisah)
    
    def test_using_home_html(self):
        response = Client().get('/kisahcovid/')
        self.assertTemplateUsed(response, 'formkisah.html')

    def test_header_ada(self):
        response = Client().get('/kisahcovid/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Tuliskan kisahmu", html_kembalian)
        self.assertTemplateUsed(response, 'formkisah.html')

    def test_title_ada(self):
        response = Client().get('/kisahcovid/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Kisah Covid", html_kembalian)
        self.assertTemplateUsed(response, 'formkisah.html')

    def test_get_kisah(self):
        response = Client().get('/kisahcovid/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Tuliskan kisahmu", html_kembalian)
        self.assertIn("Nama:", html_kembalian)
        self.assertIn("Cerita:", html_kembalian)

    def test_apakah_model_kisah_bekerja(self):
	    Kisah.objects.create(nama="pewe", cerita="Ini cerita")
	    hitung_berapa_objectnya = Kisah.objects.all().count()
	    self.assertEquals(hitung_berapa_objectnya, 1)

    def test_post_kisah(self):
        Client().post('/kisahcovid/', {'nama': 'INI NAMA', 'cerita': 'INI PESAN'} )
        count_kisah = Kisah.objects.all().count()
        self.assertEqual(count_kisah, 1)
