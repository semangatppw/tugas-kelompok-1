**Link HerokuApp :**
http://tugaskelompok-pertama.herokuapp.com

Anggota kelompok 
-Aulia Radityatama Suhendra (1906399865)
-Fareeha Nisa Zayda Azeeza (1906399644)
-Fariz Wahyuzan Dwitilas (1906399511)
-Sultan Daffa Nusantara (1906399581)
-Ian Andersen Ng (1906400280)

**Cerita aplikasi yang diajukan serta kebermanfaatannya**
Kami membuat website yang menyediakan berbagai informasi tentang COVID-19 
yang sedang mewabah sekarang. Kami menyediakan hotline . Kami juga menyediakan 
form yang dapat diisi pengunjung jika mereka menginginkan bantuan. 
Website kami menyediakan peta indonesia yang memperlihatkan zona-zona yang 
mempunyai persentase penularan yang tinggi 

**Daftar fitur yang akan diimplementasikan**
1. Home: Halaman landing dan bagian get in touch untuk menuliskan pertanyaan anda seputar covid kepada kami.
2. Kisah Covid: halaman untuk curhat mengenai cerita-cerita yang terjadi di masa pandemi
3. Feedback: Halaman untuk memberikan feedback terhadap website kami
4. Volunteer: Halaman untuk mendaftarkan diri menjadi volunteer COVID-19.
5. Donasi: Halaman untuk memberikan donasi kepada para tenaga medis COVID-19.