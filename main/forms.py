from django import forms

class feedbackform(forms.Form):
    nama = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Masukkan namamu',
        'type' : 'text',
        'required' : True
    }))
    feedback = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Masukkan pendapatmu',
        'type' : 'text',
        'required' : True
    }))