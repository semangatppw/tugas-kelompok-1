from django.urls import resolve
from django.test import TestCase , Client
from . import forms, models
from django.apps import apps
from .apps import MainConfig
from .views import feedback_view

class UnitTestTK(TestCase):

    def test_status(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_nambah_atau_tidak_models(self):
        models.feedback.objects.create(nama = "Adebe",feedback = "Website yang bagus")
        counter = models.feedback.objects.all().count()
        self.assertEqual(counter, 1)

    def test_model_feedback_isinya_benar_tidak(self):
        models.feedback.objects.create(nama = "Peokra",feedback = "Belajar POK hayuuu")
        feedbackku  = models.feedback.objects.get(nama = "Peokra")
        self.assertEqual(str(feedbackku.nama), "Peokra")
        self.assertEqual(str(feedbackku.feedback), "Belajar POK hayuuu")

    def test_form_nyimpannya(self):
        data = {'nama':"Tio",'feedback':"saya menyukai color palettenya"}
        feedback_form = forms.feedbackform(data=data)
        self.assertTrue(feedback_form.is_valid())
        self.assertEqual(feedback_form.cleaned_data['nama'],"Tio")
        self.assertEqual(feedback_form.cleaned_data['feedback'],"saya menyukai color palettenya")
    
    def test_form_post(self):
        response_post = Client().post('/feedback/', {'nama':"Reha",'feedback':"saya suka warna hijaunya"})
        counter = models.feedback.objects.all().count()
        self.assertEqual(response_post.status_code,200)
        self.assertEqual(counter, 1)
    
    def test_apps(self):
        self.assertEqual(MainConfig.name, 'main')
        self.assertEqual(apps.get_app_config('main').name, 'main') 

    def test_get(self):
        response = Client().get('/feedback/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Feedback Tentang Website Kami", html_kembalian)
        self.assertIn("Nama:", html_kembalian)
        self.assertIn("Feedback:", html_kembalian)
    
    def test_using_func(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, feedback_view)
