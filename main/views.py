from django.shortcuts import render,redirect
from . import forms, models


def feedback_view(request):
    if(request.method == "POST"):
        formku = forms.feedbackform(request.POST)
        if(formku.is_valid()):
            model_feedback = models.feedback()
            model_feedback.nama = formku.cleaned_data["nama"]
            model_feedback.feedback = formku.cleaned_data["feedback"]
            model_feedback.save()
        
    formku = forms.feedbackform()
    jumlah_input = models.feedback.objects.count()
    context = {
        'formulir' : formku,
        'jumlah_feedback' : jumlah_input
    }
    return render(request, 'main/home.html', context)