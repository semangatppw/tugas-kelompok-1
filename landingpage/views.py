from django.shortcuts import render

# Models
from .models import ContactUs

# Forms
from .forms import ContactForm

def index(request):
    if (request.method == 'POST'):
        form =  ContactForm(request.POST)
        if (form.is_valid()):
            modelku = ContactUs()    
            modelku.name = form.cleaned_data['name'],
            modelku.email = form.cleaned_data['email'],
            modelku.phone = form.cleaned_data['phone'],
            modelku.description = form.cleaned_data['description'],
            modelku.age = form.cleaned_data['age'],
            modelku.save()
            
    form = ContactForm()
    jumlah_input = ContactUs.objects.count()
    context = {
        'form': form,
        'jumlah_input' : jumlah_input
    }

    return render(request, 'index.html', context)


        
def thanks(request):
    
    jumlah_input = ContactUs.objects.count()
    context = {
       'jumlah_input' : jumlah_input
    }

    return render(request, 'thankyou.html', context)






