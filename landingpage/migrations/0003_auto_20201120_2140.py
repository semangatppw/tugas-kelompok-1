# Generated by Django 3.1.2 on 2020-11-20 14:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landingpage', '0002_auto_20201120_2057'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactus',
            name='description',
            field=models.CharField(max_length=500),
        ),
    ]
