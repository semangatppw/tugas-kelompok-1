from django.test import TestCase , Client
from django.http import HttpRequest
from . import forms, models
from django.apps import apps
from django.urls import resolve

# views
from . import views

# Models
from .models import ContactUs

class GeneralTest(TestCase):
  def test_does_pipeline_work(self):
    self.assertEquals(1, 1)

  def test_status(self):
    response = Client().get('')
    self.assertEqual(response.status_code, 200)

class TestURLS(TestCase):
  # Testing urls
  def test_base_url_exists(self):
    response = Client().get('')
    self.assertEquals(response.status_code, 200)

  def test_base_url_template(self):
    response = self.client.get('')
    self.assertTemplateUsed(response, 'index.html')

class TestViews(TestCase):
  # Testing views
  def test_index_view_response(self):
    response = views.index(HttpRequest())
    self.assertEquals(response.status_code, 200)

  def test_using_func(self):
      found = resolve('/')
      self.assertEqual(found.func, views.index)
  

class TestModels(TestCase):
  # Testing models
  def test_create_contactus(self):
    new_response = ContactUs(name='name', email='email', phone='phone', age='age', description='description')
    new_response.save()
    self.assertEquals(ContactUs.objects.all().count(), 1)
  
class TestForm(TestCase):
  #Testing forms
  def test_form_post(self):
      response_post = Client().post('', {'name':"Ian",'email':"ian@ui.ac.id", 'phone':"0123456789", 'age':"19", 'description':"Sakit"})
      counter = ContactUs.objects.all().count()
      self.assertEqual(response_post.status_code,200)
      self.assertEqual(counter, 1)

  
  
  





