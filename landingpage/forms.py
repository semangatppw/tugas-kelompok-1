from django import forms
from .models import ContactUs

class ContactForm(forms.Form):
    
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Masukkan namamu',
        'type' : 'text',
        'required' : True
    }))
    
    email = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Masukkan email',
        'type' : 'text',
        'required' : True
    }))
    
    phone = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Masukkan telepon',
        'type' : 'text',
        'required' : True
    }))
    
    age = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Masukkan umur',
        'type' : 'text',
        'required' : True
    }))

    description = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Masukkan deskripsi',
        'type' : 'text',
        'required' : True
    }))