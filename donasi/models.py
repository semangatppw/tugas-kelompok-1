from django.db import models
from django.forms import ModelForm


class Donasi (models.Model):
    nama = models.CharField("Nama Donatur", max_length=100)
    nomor = models.PositiveBigIntegerField("Nomor Rekening")
    nominal = models.PositiveBigIntegerField("Nominal Donasi")

class Form_Donasi (ModelForm):
    class Meta :
        model = Donasi
        fields = ["nama", "nomor", "nominal"]
# Create your models here.
