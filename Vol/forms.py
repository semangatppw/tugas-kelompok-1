from django import forms
from .models import Volunteer, RumahSakit

class FormRumahSakit(forms.ModelForm) :
    class Meta:
        model = RumahSakit
        fields = [
            'nama_Rumah_Sakit',
            'alamat',
            'telepon',
            'gambar',
        ]

        widgets = {
            'nama_Rumah_Sakit' : forms.TextInput(attrs={'class' : 'form-control'}),
            'alamat' : forms.TextInput(attrs={'class' : 'form-control'}),
            'telepon' : forms.NumberInput(attrs={'class' : 'form-control'}),
        }

class FormVolunteer(forms.ModelForm) :
    class Meta:
        model = Volunteer
        fields = [
            'nama_Volunteer',
            'KTP',
            'alasan',
        ]

        widgets = {
            'nama_Volunteer' : forms.TextInput(attrs={'class' : 'form-control'}),
            'KTP' : forms.TextInput(attrs={'class' : 'form-control'}),
            'alasan' : forms.Textarea(attrs={'class' : 'form-control', 'rows':4}),
        }