from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.core.files.uploadedfile import SimpleUploadedFile
import tempfile
from .views import *
from .models import *
from .forms import *

class UnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.p = RumahSakit.objects.create(nama_Rumah_Sakit="RSH", alamat="jln.", telepon="0233", gambar=tempfile.NamedTemporaryFile(suffix=".jpg").name)
        self.home = reverse("Vol:home")
        self.rumahsakit = reverse("Vol:rumahsakit")
        self.volunteer = reverse("Vol:volunteer", args=[self.p.id])

    def test_GET_home(self):
        response = self.client.get(self.home)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "home.html")

    def test_GET_volunteer(self):
        response = self.client.get(self.volunteer)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "tambahVolunteer.html")

    def test_GET_rumah_sakit(self):
        response = self.client.get(self.rumahsakit)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "tambahRS.html")

    def test_POST_volunteer(self):
        response = self.client.post(self.volunteer, 
        {
            'nama_Volunteer': 'Sultan',
            'KTP': '332212121',
            'alasan': 'Saya ingin menjadi orang terdepan melawan Covid',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "home.html")   